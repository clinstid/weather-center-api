run/dev:
	FLASK_APP=weathercenterapi FLASK_ENV=development flask run -h localhost -p 3000

black:
	black weathercenterapi/*.py

deploy:
	git push origin master
	git push heroku master
