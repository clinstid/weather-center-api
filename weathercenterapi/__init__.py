import os
from threading import Thread
import hmac
import hashlib

from flask import Flask, request, Response, make_response

from .slack import respond_with_weather, respond_with_forecast


app = Flask(__name__, instance_relative_config=True)

slack_signing_secret = os.getenv("SLACK_SIGNING_SECRET")

# ensure the instance folder exists
try:
    os.makedirs(app.instance_path)
except OSError:
    pass


def is_valid_slack_request(request):
    request_data = request.get_data().decode("utf8")
    timestamp = request.headers.get("X-Slack-Request-Timestamp")
    if not timestamp:
        return False

    sig_basestring = "v0:" + timestamp + ":" + request_data

    if not slack_signing_secret:
        return False

    m = hmac.new(slack_signing_secret.encode("utf8"), digestmod=hashlib.sha256)
    m.update(sig_basestring.encode("utf8"))
    my_signature = "v0=" + m.hexdigest()
    slack_signature = request.headers["X-Slack-Signature"]

    return my_signature == slack_signature


# Look up weather for location for next 24 hours
@app.route("/weather", methods=["POST"])
def weather():
    if not is_valid_slack_request(request):
        return make_response("Invalid request signature", 403)

    location_str = request.form["text"].strip()
    thread = Thread(
        target=respond_with_weather,
        kwargs={
            "user_id": request.form["user_id"],
            "location": location_str,
            "response_url": request.form["response_url"],
        },
    )
    thread.start()
    return "Looking up weather for {} ...".format(location_str)


# Look up weather forecast for next X days
@app.route("/forecast", methods=["POST"])
def forecast():
    if not is_valid_slack_request(request):
        return make_response("Invalid request signature", 403)

    location_str = request.form["text"].strip()
    thread = Thread(
        target=respond_with_forecast,
        kwargs={
            "user_id": request.form["user_id"],
            "location": location_str,
            "response_url": request.form["response_url"],
        },
    )
    thread.start()
    return "Looking up forecast for {} ...".format(location_str)
