"""
Location module with classes and support functions for location
"""
from threading import RLock

import requests
from pytz import timezone, utc
from timezonefinder import TimezoneFinder


class Location:
    """
    Location class used for translating location strings into geographical locations
    """

    location_cache = {}
    _cache_lock = RLock()

    def __init__(self):
        self.zip_code = None
        self.query_params = None
        self.lat = None
        self.lon = None
        self.name = None

    @classmethod
    def add_location_response(cls, location, location_response):
        """Add a new location to the location cache"""
        with cls._cache_lock:
            cls.location_cache[location] = location_response

    @classmethod
    def get_location_response(cls, location):
        """Try to retrieve the location from the location cache.

        Args:
            location: location string to search for

        Returns:
            Location object if the location is found, None if it's not
        """
        with cls._cache_lock:
            if location in cls.location_cache:
                return cls.location_cache[location]

        return None

    @classmethod
    def get_location_id(cls, gapi_key, location):
        """Get a Location object with the specified location string

        Args:
            gapi_key: Google API key for interacting with the Google location API
            location: location string to look for

        Returns:
            Location object for the specific location string
        """
        if gapi_key is None:
            raise Exception("gapi_key cannot be None!")

        cached_location = cls.get_location_response(location)
        if cached_location is not None:
            return cached_location

        # Try figuring stuff out using google's geocoding
        url = "https://maps.googleapis.com/maps/api/geocode/json"
        response = requests.get(url, params={"address": location, "key": gapi_key})

        location_response = Location()
        if response.status_code == 200:
            body = response.json()
            results = body.get("results")
            if results:
                result = results[0]
                location_response.name = result.get("formatted_address", location)
                geometry = result.get("geometry")
                if geometry is not None and "location" in geometry:
                    location_response.lat = geometry["location"]["lat"]
                    location_response.lon = geometry["location"]["lng"]
                    # We only return from here if we got all of the location
                    # identification data. If we didn't get lat/lng, then we'll
                    # fall back to the raw location (or zip code) and hope that
                    # OpenWeatherMap can figure it out.
                    cls.location_cache[location] = location_response
                    return location_response

        # If the geocoding failed, then attempt a zip code, otherwise return None because we
        # couldn't find the location.
        try:
            zip_code = int(location)
        except ValueError:
            return None
        else:
            location_response.zip_code = zip_code

        cls.add_location_response(location, location_response)
        return location_response

    def get_tz_for_geo(self):
        """Retrieve the timezone for a geographical location"""
        # TimezoneFinder relies on lon and lat so if we don't have those just return the UTC time
        # zone
        if self.lon is None or self.lat is None:
            return utc
        tz_finder = TimezoneFinder()
        return timezone(tz_finder.timezone_at(lng=self.lon, lat=self.lat))

    def get_query_param(self):
        """Build the query paramaters for the location for use with the weather api"""
        query_params = None
        if self.lat is not None and self.lon is not None:
            query_params = {"lat": self.lat, "lon": self.lon}
        elif self.zip_code is not None:
            query_params = {"zip": self.zip_code}
        else:
            query_params = {"q": self.query_params}
        return query_params
