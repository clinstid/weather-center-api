#!/usr/bin/env python3
"""Print information retrieved about a given location"""
import sys
import os

from location import Location


def main():
    """Look up the specified location string"""
    gapi_key = os.getenv("GAPI_KEY")
    location_str = sys.argv[1]

    print(f'Looking up "{location_str}"...')
    location_id = Location.get_location_id(gapi_key, location_str)
    print("location_id " + str(location_id.__dict__))


if __name__ == "__main__":
    main()
