#!/usr/bin/env python3
"""Print weather information retrieved about a given location"""
import sys
import os

import requests

from location import Location
from weather import OpenWeatherMapClient


def main():
    """Look up the specified location string"""
    gapi_key = os.getenv("GAPI_KEY")
    appid = os.getenv("OPEN_WEATHER_MAP_APP_ID")
    location_str = sys.argv[1]

    print(f'Looking up "{location_str}"...')
    location_id = Location.get_location_id(gapi_key, location_str)
    print("location_id " + str(location_id.__dict__))

    location_qp = location_id.get_query_param()
    #  location_tz = location_id.get_tz_for_geo()

    weather_client = OpenWeatherMapClient(appid)
    current_weather = None
    forecast = None

    try:
        current_weather = weather_client.get_current_weather_for_location(location_qp)
    except requests.HTTPError as ex:
        if ex.response.status_code == 404:
            reason = "location not found"
        else:
            reason = "internal error"
        print("Failed to load current weather: {}".format(reason))
        raise

    try:
        forecast = weather_client.get_forecast_for_location(location_qp)
    except requests.HTTPError as ex:
        if ex.response.status_code == 404:
            reason = "location not found"
        else:
            reason = "internal error"
        print("Failed to load forecast: {}".format(reason))
        raise

    print("current: {}".format(current_weather))
    print("forecast: {}".format(forecast))


if __name__ == "__main__":
    main()
