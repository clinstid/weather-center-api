"""Module for interactions with the Slack API"""
import json
import os
from datetime import datetime, timedelta

import requests
from pytz import utc

from .location import Location
from .weather import OpenWeatherMapClient, process_24_hour_forecast, get_wind_dir

DEGREE_SYMBOL = "°"

# Map from OpenWeatherMap's weather icon codes to default slack emoji strings. Icon codes are at:
# https://openweathermap.org/weather-conditions
WEATHER_CODE_TO_EMOJI_MAP = {
    "11d": "thunder_cloud_and_rain",
    "11n": "thunder_cloud_and_rain",
    "09d": "rain_cloud",
    "09n": "rain_cloud",
    "10d": "rain_cloud",
    "10n": "rain_cloud",
    "13d": "snow_cloud",
    "13n": "snow_cloud",
    "50d": "fog",
    "50n": "fog",
    "01d": "sunny",
    "01n": "moon",
    "02d": "partly_sunny",
    "02n": "cloud",
    "03d": "cloud",
    "03n": "cloud",
    "04d": "cloud",
    "04n": "cloud",
}


# Cache weather results by location string.
#
# Values are dicts with the following keys:
#   current_weather: Current weather response
#   forecast_data: Pre-processed 24 hour forecast
#   dt: datetime that the weather data was from
_WEATHER_CACHE = {}


def get_weather_from_cache(location):
    """Get weather data from cache

    The weather data is invalidated if it's more than 30 minutes old.

    Args:
        location: Location string

    Returns:
        A tuple with two elements:
            current_weather: The current weather response
            forecast_data: The processed 24 hour forecast data
    """
    cached_data = _WEATHER_CACHE.get(location)
    if cached_data is None:
        return (None, None)

    print("Location {location} found in cache".format(location=location))

    if cached_data.get("dt") < datetime.utcnow() - timedelta(minutes=30):
        print("Location {location} cached data is stale".format(location=location))
        del _WEATHER_CACHE[location]
        return (None, None)

    print("Returning cached data for {location}".format(location=location))
    return (cached_data.get("current_weather"), cached_data.get("forecast_data"))


def add_weather_to_cache(location, current_weather, forecast_data):
    """Add weather data to cache

    Args:
        location: Location string for the weather data
        current_weather: The current weather response
        forecast_data: The processed 24 hour forecast data
    """
    _WEATHER_CACHE[location] = {
        "current_weather": current_weather,
        "forecast_data": forecast_data,
        "dt": datetime.utcnow(),
    }


def respond_with_weather(user_id, location, response_url):
    location = location.strip()
    print("location: {}".format(location))
    print("response_url: {}".format(response_url))

    gapi_key = os.getenv("GAPI_KEY")
    appid = os.getenv("OPEN_WEATHER_MAP_APP_ID")

    print(f'Looking up "{location}"...')
    location_id = Location.get_location_id(gapi_key, location)
    if location_id is None:
        requests.post(
            response_url,
            json={
                "response_type": "ephemeral",
                "text": 'Location "{}" not found.'.format(location),
            },
        )
        return
    print("location_id " + str(location_id.__dict__))

    location_qp = location_id.get_query_param()
    location_tz = location_id.get_tz_for_geo()

    weather_client = OpenWeatherMapClient(appid)

    (current_weather, forecast_data) = get_weather_from_cache(location_id.name)

    if current_weather is None or forecast_data is None:
        try:
            current_weather = weather_client.get_current_weather_for_location(
                location_qp
            )
        except requests.HTTPError as ex:
            if ex.response.status_code == 404:
                reason = "location not found"
            else:
                reason = "internal error"
            print("Failed to load current weather: {}".format(reason))
            raise

        try:
            forecast = weather_client.get_forecast_for_location(location_qp)
        except requests.HTTPError as ex:
            if ex.response.status_code == 404:
                reason = "location not found"
            else:
                reason = "internal error"
            print("Failed to load forecast: {}".format(reason))
            raise

        forecast_data = process_24_hour_forecast(forecast.get("list"))
        add_weather_to_cache(location_id.name, current_weather, forecast_data)

    main = current_weather["main"]
    reply = (
        "<@{user_id}>: Currently at {loc} the temperature is {temp}{deg}F. Humidity: "
        "{humidity}%.".format(
            user_id=user_id,
            loc=location_id.name,
            temp=main["temp"],
            deg=DEGREE_SYMBOL,
            humidity=main["humidity"],
        )
    )

    wind = current_weather["wind"]
    if "deg" in wind:
        wind_str = " Wind is {wind_speed}mph {wind_dir} {wind_deg}{deg}.".format(
            deg=DEGREE_SYMBOL,
            wind_dir=get_wind_dir(wind["deg"]),
            wind_deg=wind["deg"],
            wind_speed=wind["speed"],
        )
    else:
        wind_str = " Wind is {wind_speed}mph.".format(wind_speed=wind["speed"])

    reply += wind_str

    fields = []
    for forecast in forecast_data["forecasts"]:
        fields.append(
            {
                "title": utc.localize(forecast.dt)
                .astimezone(location_tz)
                .strftime("%I:%M %p"),
                "value": ":{emoji}: {temp:0.2f}{deg}F, {humidity:2.0f}% humidity, {description}".format(
                    temp=forecast.temp,
                    deg=DEGREE_SYMBOL,
                    humidity=forecast.humidity,
                    description=forecast.description,
                    emoji=WEATHER_CODE_TO_EMOJI_MAP.get(
                        forecast.weather_code, "grey_question"
                    ),
                ),
                "short": True,
            }
        )

    attachments = [{"title": "Forecast for the next 24 hours", "fields": fields}]

    requests.post(
        response_url,
        json={"response_type": "in_channel", "text": reply, "attachments": attachments},
    )


def get_emoji_for_temp(temp):
    if temp < 40:
        return ":cold_face:"

    if temp < 60:
        return ":coat:"

    if temp < 70:
        return ":shirt:"

    if temp < 80:
        return ":palm_tree:"

    return ":hot_face:"


def get_emoji_for_humidity(humidity):
    if humidity < 30:
        return ":desert:"

    if humidity < 40:
        return ":deciduous_tree:"

    return ":droplet:"


def respond_with_forecast(user_id, location, response_url):
    location = location.strip()
    print("location: {}".format(location))
    print("response_url: {}".format(response_url))

    gapi_key = os.getenv("GAPI_KEY")
    appid = os.getenv("OPEN_WEATHER_MAP_APP_ID")

    print(f'Looking up "{location}"...')
    location_id = Location.get_location_id(gapi_key, location)
    if location_id is None:
        requests.post(
            response_url,
            json={
                "response_type": "ephemeral",
                "text": 'Location "{}" not found.'.format(location),
            },
        )
        return
    print("location_id " + str(location_id.__dict__))

    location_qp = location_id.get_query_param()
    location_tz = location_id.get_tz_for_geo()

    weather_client = OpenWeatherMapClient(appid)

    try:
        forecast_response = weather_client.get_16_day_forecast_for_location(location_qp)
    except requests.HTTPError as ex:
        if ex.response.status_code == 404:
            reason = "location not found"
        else:
            reason = "internal error"
        print("Failed to load forecast: {}".format(reason))
        raise

    sections = []
    fields = []
    FIELD_TEXT_TEMPLATE = "*{day}* {date}\n:{emoji}: _{description}_\n{high_emoji} {high:0.2f}{deg}F / {low_emoji} {low:0.2f}{deg}F\n{humidity_emoji} {humidity:2.0f}% humidity"
    for forecast in forecast_response["daily"]:
        dt = datetime.fromtimestamp(forecast["dt"], location_tz)
        high_emoji = get_emoji_for_temp(forecast["temp"]["max"])
        low_emoji = get_emoji_for_temp(forecast["temp"]["min"])
        humidity_emoji = get_emoji_for_humidity(forecast["humidity"])

        field = {
            "type": "mrkdwn",
            "text": FIELD_TEXT_TEMPLATE.format(
                day=dt.strftime("%A"),
                date=dt.strftime("%b %d"),
                description=forecast["weather"][0]["description"],
                emoji=WEATHER_CODE_TO_EMOJI_MAP.get(
                    forecast["weather"][0]["icon"], "grey_question"
                ),
                high=forecast["temp"]["max"],
                high_emoji=high_emoji,
                low_emoji=low_emoji,
                low=forecast["temp"]["min"],
                deg=DEGREE_SYMBOL,
                humidity_emoji=humidity_emoji,
                humidity=forecast["humidity"],
            ),
        }

        if len(fields) == 0:
            fields.append(field)
        elif len(fields) == 1:
            fields.append(field)
            sections.append({"type": "section", "fields": fields})
            fields = []

    if len(fields) > 0:
        sections.append({"type": "section", "fields": fields})

    blocks = [
        {
            "type": "header",
            "text": {
                "type": "plain_text",
                "text": "Forecast for the next {count} days for {location}".format(
                    count=len(forecast_response["daily"]), location=location_id.name
                ),
                "emoji": True,
            },
        }
    ]
    blocks.extend(sections)

    blocks_string = json.dumps(blocks)
    print(blocks_string)
    response = requests.post(
        response_url, json={"response_type": "in_channel", "blocks": blocks_string}
    )
    print(response.text)
