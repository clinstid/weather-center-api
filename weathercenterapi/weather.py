"""Module for interacting with the OpenWeatherMap API"""
from datetime import datetime, timedelta

import requests


class OpenWeatherMapClient:
    """Client class for interacting with OpenWeatherMap API"""

    BASE_WEATHER_URL = "https://api.openweathermap.org/data/2.5/{}"
    BASE_WEATHER_ICON_URL = "https://openweathermap.org/img/w/{}.png"

    def __init__(self, appid):
        """Initialize an OpenWeatherMapClient object

        Args:
            appid: Application ID from OpenWeatherMap
        """
        if appid is None:
            raise Exception("appid cannot be None!")

        self.appid = appid

    @classmethod
    def get_weather_icon_url(cls, code):
        """Build the URL for a weather icon using the specified code"""
        return cls.BASE_WEATHER_ICON_URL.format(code)

    def get_current_weather_for_location(self, location_qp):
        """Retrieve current weather for the specified location

        Args:
            location_qp: A dict containing the location data

        Returns:
            A dict containing the response from the API.

        Raises:
            Raises requests.HTTPError if the response contained a non-success HTTP status code.
        """
        query_params = {"appid": self.appid, "units": "imperial"}

        query_params.update(location_qp)

        response = requests.get(
            self.BASE_WEATHER_URL.format("weather"), params=query_params
        )
        response.raise_for_status()
        return response.json()

    def get_forecast_for_location(self, location_qp):
        """Retrieve weather forecast for the specified location

        Args:
            location_qp: A dict containing the location data

        Returns:
            A dict containing the response from the API.

        Raises:
            Raises requests.HTTPError if the response contained a non-success HTTP status code.
        """
        query_params = {"appid": self.appid, "units": "imperial"}

        query_params.update(location_qp)

        response = requests.get(
            self.BASE_WEATHER_URL.format("forecast"), params=query_params
        )
        response.raise_for_status()
        return response.json()

    def get_16_day_forecast_for_location(self, location_qp):
        """Retrieve weather forecast for the specified location for the next 16 days

        Args:
            location_qp: A dict containing the location data

        Returns:
            A dict containing the response from the API.

        Raises:
            Raises requests.HTTPError if the response contained a non-success HTTP status code.
        """
        query_params = {
            "appid": self.appid,
            "units": "imperial",
            "exclude": "current,minutely,hourly,alerts",
        }

        query_params.update(location_qp)

        response = requests.get(
            self.BASE_WEATHER_URL.format("onecall"), params=query_params
        )
        response.raise_for_status()
        return response.json()


class ForecastPoint:
    """Data class for storing one of the forecast points

    Args:
        dt: datetime object for the point of the forecast data
        temp: The temperature as an int
        humidity: The humidity as an int
        description: String description of the forecast
        weather_code: A code that represents the type of weather
    """

    def __init__(self, dt, temp, humidity, description, weather_code):
        self.dt = dt
        self.temp = temp
        self.humidity = humidity
        self.description = description
        self.weather_code = weather_code


def process_24_hour_forecast(data_points):
    """Process a 24 hour forecast and return calculated data

    Args:
        data_points: List of dicts that should contain the following fields:
            dt: datetime of the forecast data
            main: the main body of the forecast data
            weather: weather body that includes the textual description of the weather

    Returns:
        A dict with the following fields:
            min: The minimum temperature over the next 24 hours
            max: The maximum temperature over the next 24 hours
            avg: The average temperature over the next 24 hours
    """
    temps = []
    now = datetime.utcnow()
    tomorrow = now + timedelta(hours=24)
    forecast_points = []
    for data_point in data_points:
        dt = datetime.fromtimestamp(data_point["dt"])
        if dt < now:
            continue
        if dt >= tomorrow:
            break

        main = data_point.get("main")
        temp = main.get("temp")
        temps.append(temp)

        weather = data_point.get("weather")[0]
        forecast_points.append(
            ForecastPoint(
                dt=dt,
                temp=temp,
                humidity=main.get("humidity"),
                description=weather.get("description"),
                weather_code=weather.get("icon"),
            )
        )

    return {
        "min": min(temps),
        "max": max(temps),
        "avg": sum(temps) / float(len(temps)),
        "forecasts": forecast_points,
    }


def get_wind_dir(deg):
    """
    Approximate the wind direction name based on the specified degrees

    Args:
        deg: Degrees that represent the wind direction

    Returns:
        A one or two letter string that approximates the wind direction
            45: NE
            90: E
            135: SE
            180: S
            225: SW
            270: W
            315: NW
            360: N
    """
    direction = ""
    if deg <= 20 or deg >= 340:
        direction = "N"
    elif deg <= 60:
        direction = "NE"
    elif deg <= 110:
        direction = "E"
    elif deg <= 160:
        direction = "SE"
    elif deg <= 200:
        direction = "S"
    elif deg <= 240:
        direction = "SW"
    elif deg <= 290:
        direction = "W"
    elif deg <= 340:
        direction = "NW"

    return direction
